var app = new Vue({
    el: '#app',
    data: {
        pickers: [
            {emoji: "wolf", color: "ff691f"},
            {emoji: "cat", color: "fab81e"},
            {emoji: "tiger", color: "7fdbb6"},
            {emoji: "fox", color: "19cf86"},
            {emoji: "mouse", color: "91d2fa"},
            {emoji: "javali", color: "1b95e0"},
            {emoji: "pig", color: "abb8c2"},
            {emoji: "zebra", color: "e81c4f"},
            {emoji: "frog", color: "f58ea8"},
            {emoji: "bear", color: "981ceb"},
            {emoji: "koala", color: "ffffff"},
            {emoji: "rabbit", color: "000000"},
        ],
        selectedColor: 'black'
    },
    methods: {
        changeBackground: function (picker) {
            this.selectedColor = picker.color
            console.log(this.selectedColor)
        }
    }
})