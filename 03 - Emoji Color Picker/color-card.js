Vue.component('color-card', {
    props: ['name', 'colorCode', 'onClick'],
    data: () => {
        return {

        }
    },
    template: 
    `
    <div class="color-card" v-on:click="onClick">
        <div v-html=emojify(name,colorCode)></div>
        <div v-html=color(colorCode)></div>
    </div>
    `,
    methods: {
        emojify: (name, colorCode) => {
            return `<img style="background-color:#`+ colorCode +`" src="../imgs/`+ name +`.png">`
        },
        color: (colorCode) => {
            return `<div style="color:#`+ colorCode +`">#`+ colorCode + `</div>`
        }
    }
})