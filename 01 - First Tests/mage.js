Vue.component('mage', {
    props: ['spell', 'damage'],
    template: `<div v-html="emojify('mage') + ' ' + emojify(spell) + ' X' + (damage || 0) "></div>`,
    methods: {
        emojify: (name) => {
            return `<img src="../imgs/` + name + `.png">`
        }
    }
})