var app = new Vue({ 
    el: '#app',
    data: {
        message: 'Hello Vue!',
        otherMessage: "Understanding Vue", 
        emoji: '',//app.emojify("smille_crazy"),
        visible: true,
        paragraphs: [
            "Escrevendo uma mensagem em Vue.js",
            "Modificando um atributo",
            "Trabalhando com v-for"
        ],
        radioBtn: "2"
    },
    methods: {
        toogle: () => {
            app.visible = !app.visible
        },
        emojify: (name) => {
            return `<img src="../imgs/` + name + `.png">`
        }
    }
});

app.emoji = app.emojify("smille_crazy")