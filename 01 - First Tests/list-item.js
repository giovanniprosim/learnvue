Vue.component('list-item', {
    props: ['text'],
    template: '<li>{{ text }}</li>'
})