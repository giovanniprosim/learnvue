Vue.component('sirius', {
    template: `<div class="sirius" :class="siriusImg" v-html=emojify(siriusImg) v-on:click="change"></div>`,
    data: () => {
        return {
            siriusImg: 'mage'
    }},
    methods: {
        emojify: (name) => {
            return `<img src="../imgs/` + name + `.png">`
        },
        change: function () {
            this.siriusImg = this.siriusImg === 'mage' ? 'wolf' : 'mage'
        },
    }
})